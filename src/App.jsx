import { useEffect, useReducer, useRef, useState } from 'react';
import Header from './Header';
import Content from './Content';
import Footer from './Footer';
import ItemContext, { _items, _setItems } from './Context';

const App = () => {

  useEffect(() => {
    console.log('App');
  });

  const [items, setItems] = useReducer(_setItems, _items);
  const [n, setN] = useState(900000000);
  const ref = useRef();
  const sum = Sum(n);
  const inc = () => setN(n + 1);
  const dec = () => setN(n - 1);

  const add = () => {
    const { value } = ref.current;
    setItems({ name: 'ADD', value });
    ref.current.value = '';
  }

  return (
    <ItemContext.Provider value={{ value: [items, setItems] }}>
      <section style={{ textAlign: 'center', padding: 20, backgroundColor: 'green' }}>
        <h5>App</h5>
        <Header />
        <input ref={ref} />
        <button onClick={add}>add</button>
        <hr />
        <Content />
        <hr />
        <Footer n={n} sum={sum} inc={inc} dec={dec} />
      </section>
    </ItemContext.Provider>
  )
}

const Sum = (n) => {
  console.log('Hesablanir...');
  let s = 0;
  for (let i = 0; i <= n; i++) s += i;
  return s;
}


export default App
